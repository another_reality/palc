export function findUnitSellCost (initialConfig) {
  return findUnitBuyCost(initialConfig) * (1.00 + (initialConfig.profitMargin / 1))
}

export function findUnitSellCostByValueMultiplier (initialConfig) {
  return findUnitBuyCost(initialConfig) * (1.00 + (initialConfig.profitMargin / 1)) * (1.00 + (initialConfig.valueMultiplier / 1))
}

export function findUnitBuyCost (initialConfig) {
  return initialConfig.unitSizeCost / initialConfig.unitSize
}

export function findUnitInventorySum (inventory) {
  return inventory.reduce((prev, next) => Number(prev) + Number(next.givenUnitSize), 0)
}

export function findValueInventorySum (inventory) {
  return inventory.reduce((prev, next) => Number(prev) + Number(next.takenValueCost), 0)
}

export function findRemainingUnit (inventory, initialConfig) {
  return initialConfig.unitSize - inventory.reduce((prev, next) => Number(prev) + Number(next.givenUnitSize), 0)
}

export function findSpendProgress (inventory, initialConfig) {
  return findValueInventorySum(inventory) - initialConfig.unitSizeCost
}

export function findProfitProgress (inventory, initialConfig) {
  return findValueInventorySum(inventory) - findMinTarget(inventory, initialConfig)
}

export function findMinTarget (inventory, initialConfig) {
  return initialConfig.unitSize * findUnitSellCost(initialConfig)
}

export function findMinProfit (inventory, initialConfig) {
  return findMinTarget(inventory, initialConfig) - initialConfig.unitSizeCost
}

export function inventoryProfitability (inventory, initialConfig) {
  return findValueInventorySum(inventory) - (findUnitSellCost(initialConfig) * findUnitInventorySum(inventory, initialConfig))
}

export function expectedInventoryValueBasedOnMargin (inventory, initialConfig) {
  return findUnitSellCost(initialConfig) * findUnitInventorySum(inventory, initialConfig)
}

export function unitsToReachTarget (inventory, initialConfig) {
  return Math.abs(findProfitProgress(inventory, initialConfig) / findUnitSellCost(initialConfig))
}

export function realSellUnit (inventory) {
  return findValueInventorySum(inventory) / findUnitInventorySum(inventory)
}

export function maxTarget (inventory, initialConfig) {
  return (findRemainingUnit(inventory, initialConfig) * findUnitSellCost(initialConfig)) + (findUnitInventorySum(inventory) * realSellUnit(inventory))
}

export function calculatorGetValueFromUnit (initialConfig, userInput) {
  return findUnitSellCostByValueMultiplier(initialConfig) * userInput
}

export function calculatorGiveUnitFromValue (initialConfig, userInput) {
  return userInput / findUnitSellCostByValueMultiplier(initialConfig)
}
