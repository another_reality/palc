## Palc
This inventory (profit-centric) calculator is created with dealers on mind. Palc do not collect any data, everything is stored in your client with indexedDB. 
Palc is a PWA (Progressive Web App).

> [https://palc.space](https://palc.space) A unit calculator for dealers 

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Built With

* [Vue.js](https://vuejs.org/) - The javascript framework used
* [Webpack](http://vuejs-templates.github.io/webpack/) and [vue-loader](http://vuejs.github.io/vue-loader) for app PWA build. 
For detailed explanation on how things work have a look on provided resources.
* [Typicons](https://www.s-ings.com/typicons/) - Font icons on app
* [Skeleton CSS](http://getskeleton.com/) - The CSS framework used

## TODO

* Many bug-fixes need to be implemented on the calculation part
* UI fixes and improvements
* User settings for currency, and unit selection
* Configuration and data encryption before saving on indexedDB

## 
